package org.voteflux.fluxapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.adapters.FluxSpinnerAdapter;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AddressDetailsActivity extends BaseActivity {

    EditText editPostcode, editStreetNo, editDay, editMonth, editYear;
    Spinner spinnerSuburbs, spinnerStreets;
    CheckBox checkVoter;
    TextView textVoter;

    JSONObject user, country;
    ArrayList<String> tempValues = new ArrayList<>();
    String countryCode, postcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_details);

        Bundle extras = getIntent().getExtras();

        try {
            if (getIntent().hasExtra("user")) {
                user = new JSONObject(extras.getString("user"));
            } else {
                GeneralUtils.goToWelcomeError(
                        AddressDetailsActivity.this,
                        getString(R.string.something_wrong)
                );
            }

            if (getIntent().hasExtra("country")) {
                country = new JSONObject(extras.getString("country"));
                countryCode = country.getString("countryCode");
            } else {
                GeneralUtils.goToWelcomeError(
                        AddressDetailsActivity.this,
                        getString(R.string.something_wrong)
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
            GeneralUtils.goToWelcomeError(
                    AddressDetailsActivity.this,
                    getString(R.string.something_wrong)
            );
        }

        setUpViews();
    }

    private void setUpViews() {
        progressCircle = (ProgressBar) findViewById(R.id.progress_circle);

        textVoter = (TextView) findViewById(R.id.txt_canVote);
        try {
            textVoter.setText(String.format(getString(R.string.can_vote), country.getString("country")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        editPostcode = (EditText) findViewById(R.id.edit_postcode);
        editStreetNo = (EditText) findViewById(R.id.edit_streetNo);
        editDay = (EditText) findViewById(R.id.edit_dobDay);
        editMonth = (EditText) findViewById(R.id.edit_dobMonth);
        editYear = (EditText) findViewById(R.id.edit_dobYear);

        checkVoter = (CheckBox) findViewById(R.id.check_canVote);

        tempValues.add("Loading...");
        tempValues.add("Suburb");
        tempValues.add("Street Name");

        final FluxSpinnerAdapter tempAdapter =
                new FluxSpinnerAdapter(AddressDetailsActivity.this, tempValues);

        spinnerSuburbs = (Spinner) findViewById(R.id.spin_suburbs);
        spinnerSuburbs.setAdapter(tempAdapter);
        spinnerSuburbs.setSelection(1);
        spinnerSuburbs.setEnabled(false);

        spinnerStreets = (Spinner) findViewById(R.id.spin_streets);
        spinnerStreets.setAdapter(tempAdapter);
        spinnerStreets.setSelection(2);
        spinnerStreets.setEnabled(false);

        editPostcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    spinnerSuburbs.setSelection(0);
                    postcode = s.toString();

                    FluxAPIHandler.getSuburbs(countryCode, postcode, new FluxAPIHandler.GetSuburbsCallback() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            ArrayList<String> suburbs = new ArrayList<>();

                            try {
                                JSONArray JSONsuburbs = result.getJSONArray("suburbs");
                                for (int i = 0; i < JSONsuburbs.length(); i++) {
                                    suburbs.add(JSONsuburbs.get(i).toString());
                                }

                                FluxSpinnerAdapter adapter =
                                        new FluxSpinnerAdapter(AddressDetailsActivity.this, suburbs);
                                spinnerSuburbs.setAdapter(adapter);
                                spinnerSuburbs.setEnabled(true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError() {
                            spinnerSuburbs.setSelection(1);
                            spinnerSuburbs.setEnabled(false);

                            Toast.makeText(
                                    AddressDetailsActivity.this,
                                    getString(R.string.something_wrong),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }

                        @Override
                        public void onError(JSONObject error) {
                            spinnerSuburbs.setSelection(1);
                            spinnerSuburbs.setEnabled(false);

                            Toast.makeText(
                                    AddressDetailsActivity.this,
                                    GeneralUtils.getJSONErrorMessage(AddressDetailsActivity.this, error),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    });
                } else {
                    spinnerSuburbs.setAdapter(tempAdapter);
                    spinnerSuburbs.setSelection(1);
                    spinnerSuburbs.setEnabled(false);

                    spinnerStreets.setAdapter(tempAdapter);
                    spinnerStreets.setSelection(2);
                    spinnerStreets.setEnabled(false);
                }
            }
        });

        spinnerSuburbs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!tempValues.contains(spinnerSuburbs.getSelectedItem().toString())) {
                    spinnerStreets.setSelection(0);
                    String suburb = (String) spinnerSuburbs.getSelectedItem();

                    FluxAPIHandler.getStreets(countryCode, postcode, suburb, new FluxAPIHandler.GetStreetsCallback() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            Log.d(FluxApp.TAG, result.toString());
                            ArrayList<String> streets = new ArrayList<>();

                            try {
                                JSONArray JSONstreets = result.getJSONArray("streets");
                                for (int i = 0; i < JSONstreets.length(); i++) {
                                    streets.add(JSONstreets.get(i).toString());
                                }

                                FluxSpinnerAdapter adapter =
                                        new FluxSpinnerAdapter(AddressDetailsActivity.this, streets);
                                spinnerStreets.setAdapter(adapter);
                                spinnerStreets.setEnabled(true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError() {
                            spinnerStreets.setSelection(2);
                            spinnerSuburbs.setEnabled(false);

                            Toast.makeText(
                                    AddressDetailsActivity.this,
                                    getString(R.string.something_wrong),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }

                        @Override
                        public void onError(JSONObject error) {
                            spinnerStreets.setSelection(2);
                            spinnerSuburbs.setEnabled(false);

                            Toast.makeText(
                                    AddressDetailsActivity.this,
                                    GeneralUtils.getJSONErrorMessage(AddressDetailsActivity.this, error),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView circle = (ImageView) findViewById(R.id.circle4);
        setFilledCircle(circle);
    }

    public void onRegister(View view) {
        if (checkMandatory()) {
            try {
                user.put("onAECRoll", checkVoter.isChecked());

                JSONArray validRegions = new JSONArray();
                validRegions.put(country.getString("regionCode"));
                user.put("valid_regions", validRegions);

                String dayOfBirth = editDay.getText().toString();
                if (dayOfBirth.length() == 1) {
                    dayOfBirth = "0" + dayOfBirth;
                }
                user.put("dobDay", dayOfBirth);

                String monthOfBirth = editMonth.getText().toString();
                if (monthOfBirth.length() == 1) {
                    monthOfBirth = "0" + monthOfBirth;
                }
                user.put("dobMonth", monthOfBirth);

                String yearOfBirth = editYear.getText().toString();
                user.put("dobYear", yearOfBirth);

                user.put("dob", String.format("%1$s-%2$s-%3$s", yearOfBirth, monthOfBirth, dayOfBirth));

                user.put("addr_country", countryCode);

                user.put("addr_postcode", postcode);

                String suburb = (String) spinnerSuburbs.getSelectedItem();
                user.put("addr_suburb", suburb);

                String street = (String) spinnerStreets.getSelectedItem();
                user.put("addr_street", street);

                String streetNo = editStreetNo.getText().toString();
                user.put("addr_street_no", streetNo);

                user.put("address", String.format("%1$s; %2$s; %3$s; %4$s; %5$s", streetNo, street, suburb, postcode, countryCode.toUpperCase()));

                user.put("addr_version", 1);

                progressCircle.setVisibility(View.VISIBLE);

                FluxAPIHandler.registerUser(user, new FluxAPIHandler.RegisterUserCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        progressCircle.setVisibility(View.GONE);

                        try {
                            FluxApp.getEditor().putString("s", result.getString("s")).apply();

                            Intent close = new Intent("YourDetails");
                            close.putExtra("action", "close");
                            LocalBroadcastManager.getInstance(AddressDetailsActivity.this).sendBroadcast(close);

                            Intent intent = new Intent(AddressDetailsActivity.this, CheckMembershipActivity.class);
                            intent.putExtra("user", result.getJSONObject("user").toString());
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            GeneralUtils.goToWelcomeError(
                                    AddressDetailsActivity.this,
                                    getString(R.string.something_wrong)
                            );
                        }
                    }

                    @Override
                    public void onError() {
                        progressCircle.setVisibility(View.GONE);

                        Toast.makeText(
                                AddressDetailsActivity.this,
                                getString(R.string.something_wrong),
                                Toast.LENGTH_SHORT
                        ).show();
                    }

                    @Override
                    public void onError(JSONObject error) {
                        progressCircle.setVisibility(View.GONE);

                        Toast.makeText(
                                AddressDetailsActivity.this,
                                GeneralUtils.getJSONErrorMessage(AddressDetailsActivity.this, error),
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkMandatory() {
        boolean canSubmit = true;

        String postcode = editPostcode.getText().toString();
        String suburb = (String) spinnerSuburbs.getSelectedItem();
        String street = (String) spinnerStreets.getSelectedItem();
        String streetNo = editStreetNo.getText().toString();
        String dayOfBirth = editDay.getText().toString();
        if (dayOfBirth.length() == 1) {
            dayOfBirth = "0" + dayOfBirth;
        }
        String monthOfBirth = editMonth.getText().toString();
        if (monthOfBirth.length() == 1) {
            monthOfBirth = "0" + monthOfBirth;
        }
        String yearOfBirth = editYear.getText().toString();

        if (postcode.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your postcode"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (suburb.isEmpty() || suburb.equals("Loading...") || suburb.equals("Suburb")) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your suburb"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (street.isEmpty() || street.equals("Loading...") || street.equals("Street Name")) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your street name"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (streetNo.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your street number"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (dayOfBirth.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your date of birth"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (monthOfBirth.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your date of birth"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (yearOfBirth.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your date of birth"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (!isValidDOB(dayOfBirth, monthOfBirth, yearOfBirth)) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    getString(R.string.dob_invalid),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (!checkVoter.isChecked()) {
            canSubmit = false;

            Toast.makeText(
                    AddressDetailsActivity.this,
                    getString(R.string.must_be_voter),
                    Toast.LENGTH_SHORT
            ).show();
        }

        return canSubmit;
    }

    private boolean isValidDOB(String day, String month, String year) {
        boolean isValid = true;

        int dayInt = Integer.parseInt(day);
        int monthInt = Integer.parseInt(month);
        int yearInt = Integer.parseInt(year);

        if (day.length() != 2 || month.length() != 2 || year.length() != 4) {
            isValid =  false;
        }

        switch (monthInt) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if (dayInt > 31) {
                    isValid = false;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (dayInt > 30) {
                    isValid = false;
                }
            case 2:
                if (yearInt % 4 == 0) {
                    // Leap year
                    if (dayInt > 29) {
                        isValid = false;
                    }
                } else {
                    if (dayInt > 28) {
                        isValid = false;
                    }
                }
                break;
            default:
                isValid = false;
                break;
        }

        return isValid;
    }
}
