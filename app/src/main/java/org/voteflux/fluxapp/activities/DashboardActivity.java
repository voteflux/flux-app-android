package org.voteflux.fluxapp.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.adapters.DashboardAdapter;
import org.voteflux.fluxapp.fragments.DashboardFragment;
import org.voteflux.fluxapp.fragments.FeedbackFragment;
import org.voteflux.fluxapp.fragments.VoteFragment;
import org.voteflux.fluxapp.utils.GeneralUtils;
import org.voteflux.fluxapp.views.FluxViewPager;

public class DashboardActivity extends BaseActivity {

    TabLayout tabDashboard;
    FluxViewPager pagerDashboard;

    Fragment dashboard, vote, feedback;

    JSONObject user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Bundle extras = getIntent().getExtras();

        // Confirm user object available
        if (getIntent().hasExtra("user")) {
            try {
                user = new JSONObject(extras.getString("user"));
            } catch (JSONException e) {
                e.printStackTrace();
                GeneralUtils.goToWelcomeError(DashboardActivity.this, getString(R.string.no_user));
            }
        } else {
            // There should be a user object available to access this page
            GeneralUtils.goToWelcomeError(DashboardActivity.this, getString(R.string.no_user));
        }

        setUpFragments();

        setUpViews();
    }

    @Override
    public void onBackPressed() {
        if (pagerDashboard.getCurrentItem() == 1) {
            VoteFragment voteFragment = (VoteFragment) vote;
            if (voteFragment.isShowingDetails()) {
                voteFragment.toggleDetails();
            }
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
    }

    private void setUpViews() {
        pagerDashboard = (FluxViewPager) findViewById(R.id.pager_dashboard);
        setUpViewPager(pagerDashboard);

        tabDashboard = (TabLayout) findViewById(R.id.tab_dashboard);
        tabDashboard.setupWithViewPager(pagerDashboard);
        setTabIcons();
    }

    private void setUpFragments() {
        dashboard = new DashboardFragment();
        vote = new VoteFragment();
        feedback = new FeedbackFragment();
    }

    private void setUpViewPager(FluxViewPager viewPager) {
        DashboardAdapter adapter = new DashboardAdapter(getSupportFragmentManager());
        adapter.addFragment(dashboard);
        adapter.addFragment(vote);
        adapter.addFragment(feedback);

        viewPager.setAdapter(adapter);

        viewPager.setSwipingEnabled(false);
    }

    private void setTabIcons() {
        View dashboard = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.view_tab_dashboard, null);

        ImageView dashboardIcon = (ImageView) dashboard.findViewById(R.id.tab_icon);
        dashboardIcon.setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.tab_dashboard));

        TextView dashboardText = (TextView) dashboard.findViewById(R.id.tab_text);
        dashboardText.setText(getString(R.string.dashboard));

        tabDashboard.getTabAt(0).setCustomView(dashboard);

        View vote = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.view_tab_dashboard, null);

        ImageView voteIcon = (ImageView) vote.findViewById(R.id.tab_icon);
        voteIcon.setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.tab_vote));

        TextView voteText = (TextView) vote.findViewById(R.id.tab_text);
        voteText.setText(getString(R.string.vote));

        tabDashboard.getTabAt(1).setCustomView(vote);

        View feedback = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.view_tab_dashboard, null);

        ImageView feedbackIcon = (ImageView) feedback.findViewById(R.id.tab_icon);
        feedbackIcon.setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.tab_feedback));

        TextView feedbackText = (TextView) feedback.findViewById(R.id.tab_text);
        feedbackText.setText(getString(R.string.feedback));

        tabDashboard.getTabAt(2).setCustomView(feedback);
    }

    public interface HideIssueDetailsListener {
        void hideIssueDetails();
    }
}
