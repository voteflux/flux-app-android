package org.voteflux.fluxapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.FluxAPIHandler;

public class MagicLinkActivity extends BaseActivity {

    EditText editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magic_link);

        setUpViews();
    }

    private void setUpViews() {
        editEmail = (EditText) findViewById(R.id.edit_magicEmail);
        progressCircle = (ProgressBar) findViewById(R.id.progress_circle);
    }

    public void onSendLink(View view) {
        final String email = editEmail.getText().toString();

        if (email.isEmpty()) {
            Toast.makeText(
                    MagicLinkActivity.this,
                    String.format(getString(R.string.must_enter), "your email"),
                    Toast.LENGTH_SHORT
            ).show();
        } else {
            progressCircle.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();

            try {
                params.put("email", email);

                FluxAPIHandler.sendMagicLink(params, new FluxAPIHandler.SendMagicLinkCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        progressCircle.setVisibility(View.GONE);
                        goToCheckEmail(email);
                    }

                    @Override
                    public void onError() {
                        progressCircle.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(JSONObject error) {
                        progressCircle.setVisibility(View.GONE);

                        try {
                            Toast.makeText(
                                    MagicLinkActivity.this,
                                    String.format(getString(R.string.something_wrong_reason), error.getString("error")),
                                    Toast.LENGTH_SHORT
                            ).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();

                progressCircle.setVisibility(View.GONE);
            }
        }
    }

    private void goToCheckEmail(String email) {
        Intent intent = new Intent(MagicLinkActivity.this, CheckEmailActivity.class);
        intent.putExtra("email", email);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
