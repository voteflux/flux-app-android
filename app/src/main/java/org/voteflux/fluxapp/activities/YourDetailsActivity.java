package org.voteflux.fluxapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.adapters.FluxSpinnerCountryAdapter;
import org.voteflux.fluxapp.utils.Constants;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class YourDetailsActivity extends BaseActivity {

    EditText editFirstName, editMiddleNames, editSurname, editEmail;
    Spinner spinnerCountries;

    JSONObject user, country;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");

            if (action.equals("close")) {
                YourDetailsActivity.this.finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_details);

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("YourDetails"));

        Bundle extras = getIntent().getExtras();

        try {
            if (getIntent().hasExtra("user")) {
                user = new JSONObject(extras.getString("user"));
            } else {
                GeneralUtils.goToWelcomeError(
                        YourDetailsActivity.this,
                        getString(R.string.something_wrong)
                );
            }

            if (getIntent().hasExtra("country")) {
                country = new JSONObject(extras.getString("country"));
            } else {
                GeneralUtils.goToWelcomeError(
                        YourDetailsActivity.this,
                        getString(R.string.something_wrong)
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
            GeneralUtils.goToWelcomeError(
                    YourDetailsActivity.this,
                    getString(R.string.something_wrong)
            );
        }

        setUpViews();
    }

    private void setUpViews() {
        editFirstName = (EditText) findViewById(R.id.edit_firstName);
        editMiddleNames = (EditText) findViewById(R.id.edit_middleName);
        editSurname = (EditText) findViewById(R.id.edit_surname);
        editEmail = (EditText) findViewById(R.id.edit_email);

        spinnerCountries = (Spinner) findViewById(R.id.spin_countries);

        FluxSpinnerCountryAdapter adapter =
                new FluxSpinnerCountryAdapter(this, Constants.getCountries());
        spinnerCountries.setAdapter(adapter);

        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = (JSONObject) spinnerCountries.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            int position = getCountryPosition(country, spinnerCountries);

            if (position >= 0) {
                spinnerCountries.setSelection(position);
            } else {
                country = (JSONObject) spinnerCountries.getItemAtPosition(0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ImageView circle = (ImageView) findViewById(R.id.circle3);
        setFilledCircle(circle);
    }

    private int getCountryPosition(JSONObject country, Spinner spinner) throws JSONException {
        for (int i = 0; i < spinner.getCount(); i++) {
            JSONObject compareCountry = (JSONObject) spinner.getItemAtPosition(i);
            if (compareCountry.getString("countryCode").equals(country.getString("countryCode"))) {
                return i;
            }
        }

        // Not found
        return -1;
    }

    public void onNext(View view) {
        if (checkMandatory()) {
            String firstName = editFirstName.getText().toString();
            String middleNames = editMiddleNames.getText().toString();
            String surname = editSurname.getText().toString();
            String email = editEmail.getText().toString();


            try {
                user.put("fname", firstName);
                user.put("mnames", middleNames);
                user.put("sname", surname);
                user.put("name", String.format("%1$s %2$s %3$s", firstName, middleNames, surname));
                user.put("email", email);

                Intent intent = new Intent(YourDetailsActivity.this, AddressDetailsActivity.class);
                intent.putExtra("country", country.toString());
                intent.putExtra("user", user.toString());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(
                        YourDetailsActivity.this,
                        getString(R.string.something_wrong),
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    private boolean checkMandatory() {
        boolean canSubmit = true;

        String firstName = editFirstName.getText().toString();
        String surname = editSurname.getText().toString();
        String email = editEmail.getText().toString();

        if (firstName.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    YourDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your legal first name"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (surname.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    YourDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your legal surname"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        if (email.isEmpty()) {
            canSubmit = false;

            Toast.makeText(
                    YourDetailsActivity.this,
                    String.format(getString(R.string.must_enter), "your email address"),
                    Toast.LENGTH_SHORT
            ).show();
        }

        return canSubmit;
    }
}
