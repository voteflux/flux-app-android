package org.voteflux.fluxapp.activities;

import android.os.Bundle;
import android.widget.TextView;

import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class CheckEmailActivity extends BaseActivity {

    TextView textEmailSentTo, textEmailDetails;

    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_email);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("email");
        }

        setUpViews();
    }

    private void setUpViews() {
        textEmailSentTo = (TextView) findViewById(R.id.txt_emailSentTo);
        String sentTo = String.format(getString(R.string.email_sent_to), email);
        textEmailSentTo.setText(
                GeneralUtils.boldSelectedText(CheckEmailActivity.this, sentTo, email)
        );

        textEmailDetails = (TextView) findViewById(R.id.txt_emailDetails);
        textEmailDetails.setText(
                GeneralUtils.boldSelectedText(CheckEmailActivity.this, getString(R.string.email_sent_details), "Flux")
        );
    }
}
