package org.voteflux.fluxapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.adapters.FluxSpinnerCountryAdapter;
import org.voteflux.fluxapp.utils.Constants;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class EnterPhoneActivity extends BaseActivity {

    private Spinner spinnerCountries;
    private TextView mobileCode;
    private EditText mobileNumber;

    private JSONObject country;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");

            if (action.equals("close")) {
                EnterPhoneActivity.this.finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_phone);

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("EnterPhone"));

        setUpViews();
    }

    private void setUpViews() {
        progressCircle = (ProgressBar) findViewById(R.id.progress_circle);

        spinnerCountries = (Spinner) findViewById(R.id.spin_countries);
        mobileCode = (TextView) findViewById(R.id.txt_mobileCode);
        mobileNumber = (EditText) findViewById(R.id.edit_mobileNo);

        FluxSpinnerCountryAdapter adapter =
                new FluxSpinnerCountryAdapter(this, Constants.getCountries());
        spinnerCountries.setAdapter(adapter);

        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = (JSONObject) spinnerCountries.getItemAtPosition(position);

                try {
                    mobileCode.setText(country.getString("mobileCode"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView circle = (ImageView) findViewById(R.id.circle1);
        setFilledCircle(circle);
    }

    public void onSendCode(View view) {
        final String mobile = mobileCode.getText().toString() + mobileNumber.getText().toString();

        if (mobileNumber.getText().toString().isEmpty()) {
            Toast.makeText(
                    EnterPhoneActivity.this,
                    String.format(getString(R.string.must_enter), "your mobile number"),
                    Toast.LENGTH_SHORT
            ).show();
        } else {
            JSONObject params = new JSONObject();
            try {
                progressCircle.setVisibility(View.VISIBLE);

                params.put("phone_number", mobile);

                FluxAPIHandler.sendSMS(params, new FluxAPIHandler.SendSMSCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        progressCircle.setVisibility(View.GONE);

                        JSONObject user = new JSONObject();
                        try {
                            user.put("contact_number", mobile);

                            Intent intent = new Intent(EnterPhoneActivity.this, VerifyPhoneActivity.class);
                            intent.putExtra("country", country.toString());
                            intent.putExtra("user", user.toString());
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            GeneralUtils.goToWelcomeError(
                                    EnterPhoneActivity.this,
                                    getString(R.string.something_wrong)
                            );
                        }
                    }

                    @Override
                    public void onError() {
                        progressCircle.setVisibility(View.GONE);

                        Toast.makeText(
                                EnterPhoneActivity.this,
                                getString(R.string.something_wrong),
                                Toast.LENGTH_SHORT
                        ).show();
                    }

                    @Override
                    public void onError(JSONObject error) {
                        progressCircle.setVisibility(View.GONE);

                        Toast.makeText(
                                EnterPhoneActivity.this,
                                GeneralUtils.getJSONErrorMessage(EnterPhoneActivity.this, error),
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
                progressCircle.setVisibility(View.GONE);
            }
        }
    }
}
