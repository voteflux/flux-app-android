package org.voteflux.fluxapp.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.voteflux.fluxapp.R;

public class BaseActivity extends AppCompatActivity {

    ProgressBar progressCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
    }

    public void setFilledCircle(ImageView circle) {
        circle.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.circle_filled));
    }
}
