package org.voteflux.fluxapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class SplashScreenActivity extends BaseActivity implements MediaPlayer.OnCompletionListener {

    private VideoView videoSplashScreen;
    private boolean isFirstTime = true;
    private boolean isLoaded = false;
    private boolean isCompleted = false;
    private Intent nextActivity;
    private String errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        isFirstTime = FluxApp.getSharedPrefs().getBoolean("isFirstTime", true);
        Log.d(FluxApp.TAG, "isFirstTime " + isFirstTime);

        onNewIntent(getIntent());

        setUpViews();

        videoSplashScreen.setZOrderOnTop(true);
        videoSplashScreen.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        videoSplashScreen.start();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();
        String data = intent.getDataString();

        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            // Accessed via magic link -> sign in user
            String token = data.substring(data.lastIndexOf("/") + 1);
            nextActivity = new Intent(SplashScreenActivity.this, CheckMembershipActivity.class);
            nextActivity.putExtra("path", "magic_link");

            FluxAPIHandler.getUserMagicLink(token, new FluxAPIHandler.GetUserMagicLinkCallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(FluxApp.TAG, result.toString());
                    if (result.has("s")) {
                        // There is a user
                        try {
                            FluxApp.getEditor().putString("s", result.getString("s")).apply();

                            nextActivity.putExtra("user", result.getJSONObject("user").toString());

                            isLoaded = true;

                            if (isCompleted) {
                                goToNextActivity();
                            }
                        } catch (JSONException e) {
                            isLoaded = true;

                            nextActivity = null;
                            errorMessage = getString(R.string.something_wrong);

                            if (isCompleted) {
                                goToNextActivity();
                            }
                        }
                    } else {
                        isLoaded = true;

                        errorMessage = getString(R.string.something_wrong);
                        goToNextActivity();

                        if (isCompleted) {
                            nextActivity = null;
                        }
                    }
                }

                @Override
                public void onError() {
                    isLoaded = true;


                    nextActivity = null;
                    errorMessage = getString(R.string.something_wrong);

                    if (isCompleted) {
                        goToNextActivity();
                    }
                }

                @Override
                public void onError(JSONObject error) {
                    isLoaded = true;

                    nextActivity = null;
                    errorMessage = GeneralUtils.getJSONErrorMessage(SplashScreenActivity.this, error);

                    if (isCompleted) {
                        goToNextActivity();
                    }
                }
            });
        } else {
            String s = FluxApp.getSharedPrefs().getString("s", "");

            if (s.isEmpty()) {
                // User not logged in
                nextActivity = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
                nextActivity.putExtra("path", "not_logged_in");
                isLoaded = true;
            } else {
                // User already logged in on this phone,
                // Get user object and head over to the Dashboard
                nextActivity = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                nextActivity.putExtra("path", "logged_in");

                JSONObject params = new JSONObject();
                try {
                    params.put("s", s);

                    FluxAPIHandler.getUser(params, new FluxAPIHandler.GetUserCallback() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            if (result.has("s")) {
                                // There is a user
                                try {
                                    FluxApp.getEditor().putString("s", result.getString("s")).apply();

                                    nextActivity.putExtra("user", result.toString());

                                    isLoaded = true;

                                    if (isCompleted) {
                                        goToNextActivity();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    isLoaded = true;

                                    nextActivity = null;
                                    errorMessage = getString(R.string.something_wrong);

                                    if (isCompleted) {
                                        goToNextActivity();
                                    }
                                }
                            } else {
                                isLoaded = true;

                                nextActivity = null;
                                errorMessage = getString(R.string.something_wrong);

                                if (isCompleted) {
                                    goToNextActivity();
                                }
                            }
                        }

                        @Override
                        public void onError() {
                            isLoaded = true;

                            nextActivity = null;
                            errorMessage = getString(R.string.something_wrong);

                            if (isCompleted) {
                                goToNextActivity();
                            }
                        }

                        @Override
                        public void onError(JSONObject error) {
                            isLoaded = true;

                            nextActivity = null;
                            errorMessage = GeneralUtils.getJSONErrorMessage(SplashScreenActivity.this, error);

                            if (isCompleted) {
                                goToNextActivity();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    isLoaded = true;

                    nextActivity = null;
                    errorMessage = getString(R.string.something_wrong);

                    if (isCompleted) {
                        goToNextActivity();
                    }
                }
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (isFirstTime) {
            setVideo(Uri.parse(String.format("android.resource://%1$s/%2$s", getPackageName(), R.raw.splash_flux_logo)));
            videoSplashScreen.start();

            isFirstTime = false;
            FluxApp.getEditor().putBoolean("isFirstTime", isFirstTime);
            FluxApp.getEditor().apply();
        } else {
            if (isLoaded) {
                goToNextActivity();
            }
        }
    }

    private void setUpViews() {
        videoSplashScreen = (VideoView) findViewById(R.id.vid_splashScreen);

        if (isFirstTime) {
            setVideo(Uri.parse(String.format("android.resource://%1$s/%2$s", getPackageName(), R.raw.splash_words)));
        } else {
            setVideo(Uri.parse(String.format("android.resource://%1$s/%2$s", getPackageName(), R.raw.splash_flux_logo)));
        }

        videoSplashScreen.setOnCompletionListener(this);

        videoSplashScreen.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return true;
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) videoSplashScreen.getLayoutParams();
        layoutParams.width = metrics.widthPixels;
        layoutParams.height = metrics.heightPixels;
        videoSplashScreen.setLayoutParams(layoutParams);
    }

    private void setVideo(Uri uri) {
        videoSplashScreen.setVideoURI(uri);
    }

    private void goToNextActivity() {
        if (nextActivity != null) {
            startActivity(nextActivity);
        } else {
            GeneralUtils.goToWelcomeError(
                    SplashScreenActivity.this,
                    errorMessage
            );
        }

        finishAffinity();
    }

    public interface UserLoadedCallback {
        void onUserLoaded();
        void onError();
    }
}
