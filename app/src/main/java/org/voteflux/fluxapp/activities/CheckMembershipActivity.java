package org.voteflux.fluxapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.vdurmont.emoji.EmojiParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class CheckMembershipActivity extends BaseActivity {

    TextView textNowAMember, textParty, textWelcome;
    Switch switchVolunteer, switchCandidate, switchChapters, switchState;

    JSONObject user;
    String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_membership);

        Bundle extras = getIntent().getExtras();

        // Confirm user object available
        if (getIntent().hasExtra("user")) {
            try {
                user = new JSONObject(extras.getString("user"));
            } catch (JSONException e) {
                e.printStackTrace();
                GeneralUtils.goToWelcomeError(CheckMembershipActivity.this, getString(R.string.no_user));
            }
        } else {
            // There should be a user object available to access this page
            GeneralUtils.goToWelcomeError(CheckMembershipActivity.this, getString(R.string.no_user));
        }

        if (getIntent().hasExtra("path")) {
            path = extras.getString("path");
        }

        setUpViews();
    }

    private void setUpViews() {
        progressCircle = (ProgressBar) findViewById(R.id.progress_circle);

        textNowAMember = (TextView) findViewById(R.id.txt_nowAMember);
        textParty = (TextView) findViewById(R.id.txt_party);
        textWelcome = (TextView) findViewById(R.id.txt_welcome);

        textParty.setText(EmojiParser.parseToUnicode(getString(R.string.party_emoji)));

        if (path.equals("magic_link")) {
            textNowAMember.setVisibility(View.GONE);
            try {
                textWelcome.setText(String.format(getString(R.string.welcome_back), user.getString("fname")));
            } catch (JSONException e) {
                e.printStackTrace();
                textWelcome.setText(String.format(getString(R.string.welcome_back), ""));
            }
        }

        switchVolunteer = (Switch) findViewById(R.id.switch_volunteer);
        switchCandidate = (Switch) findViewById(R.id.switch_candidate);
        switchChapters = (Switch) findViewById(R.id.switch_organiseChapters);
        switchState = (Switch) findViewById(R.id.switch_memberStateBranch);

        try {
            switchVolunteer.setChecked(user.getBoolean("volunteer"));
            switchCandidate.setChecked(user.getBoolean("candidature_local") || user.getBoolean("candidature_state") || user.getBoolean("candidature_federal"));
            switchChapters.setChecked(user.getBoolean("vol_organise"));
            switchState.setChecked(user.getBoolean("state_consent"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onUpdateDetails(View view) {
        JSONObject params = new JSONObject();

        try {
            params.put("volunteer", switchVolunteer.isChecked());
            params.put("candidature_local", switchCandidate.isChecked());
            params.put("candidature_state", switchCandidate.isChecked());
            params.put("candidature_federal", switchCandidate.isChecked());
            params.put("vol_organise", switchChapters.isChecked());
            params.put("state_consent", switchState.isChecked());
            params.put("s", FluxApp.getSharedPrefs().getString("s", ""));

            progressCircle.setVisibility(View.VISIBLE);

            FluxAPIHandler.updateUserDetails(params, new FluxAPIHandler.UpdateUserDetailsCallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    progressCircle.setVisibility(View.GONE);

                    Intent intent = new Intent(CheckMembershipActivity.this, DashboardActivity.class);
                    intent.putExtra("user", result.toString());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finishAffinity();
                }

                @Override
                public void onError() {
                    progressCircle.setVisibility(View.GONE);

                    Toast.makeText(
                            CheckMembershipActivity.this,
                            getString(R.string.something_wrong),
                            Toast.LENGTH_SHORT
                    ).show();
                }

                @Override
                public void onError(JSONObject error) {
                    progressCircle.setVisibility(View.GONE);

                    Toast.makeText(
                            CheckMembershipActivity.this,
                            GeneralUtils.getJSONErrorMessage(CheckMembershipActivity.this, error),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(
                    CheckMembershipActivity.this,
                    getString(R.string.something_wrong),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
}
