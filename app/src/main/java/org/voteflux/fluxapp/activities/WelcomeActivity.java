package org.voteflux.fluxapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class WelcomeActivity extends BaseActivity {

    TextView txtAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Bundle extras = getIntent().getExtras();

        if (getIntent().hasExtra("error")) {
            // An error led us here
            Toast.makeText(WelcomeActivity.this, extras.getString("error"), Toast.LENGTH_SHORT).show();
        }

        setUpViews();
    }

    private void setUpViews() {
        txtAbout = (TextView) findViewById(R.id.txt_about);
        txtAbout.setText(GeneralUtils.boldSelectedText(WelcomeActivity.this, getString(R.string.about), "advanced", "Free", "secure"));
    }

    public void onJoin(View view) {
        Intent intent = new Intent(WelcomeActivity.this, EnterPhoneActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void onAlreadyMember(View view) {
        Intent intent = new Intent(WelcomeActivity.this, MagicLinkActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
