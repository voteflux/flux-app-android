package org.voteflux.fluxapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;

import static android.widget.Toast.LENGTH_SHORT;

public class VerifyPhoneActivity extends BaseActivity {

    EditText editCode;

    JSONObject user;
    String nonce, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        Bundle extras = getIntent().getExtras();

        if (getIntent().hasExtra("user")) {
            try {
                user = new JSONObject(extras.getString("user"));
            } catch (JSONException e) {
                e.printStackTrace();
                GeneralUtils.goToWelcomeError(
                        VerifyPhoneActivity.this,
                        getString(R.string.something_wrong)
                );
            }
        } else {
            GeneralUtils.goToWelcomeError(
                    VerifyPhoneActivity.this,
                    getString(R.string.something_wrong)
            );
        }

        if (getIntent().hasExtra("country")) {
            country = extras.getString("country");
        } else {
            GeneralUtils.goToWelcomeError(
                    VerifyPhoneActivity.this,
                    getString(R.string.something_wrong)
            );
        }

        setUpViews();
    }

    private void setUpViews() {
        progressCircle = (ProgressBar) findViewById(R.id.progress_circle);

        editCode = (EditText) findViewById(R.id.edit_smsCode);

        editCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    String code = s.toString();
                    verifyPhone(code);
                }
            }
        });

        ImageView circle = (ImageView) findViewById(R.id.circle2);
        setFilledCircle(circle);
    }

    private void verifyPhone(String code) {
        nonce = GeneralUtils.generateNonce(20);

        JSONObject params = new JSONObject();

        try {
            progressCircle.setVisibility(View.VISIBLE);

            params.put("phone_number", user.getString("contact_number"));
            params.put("verify_code", code);
            params.put("verify_nonce", nonce);

            FluxAPIHandler.verifyPhone(params, new FluxAPIHandler.VerifyPhoneCallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    progressCircle.setVisibility(View.GONE);

                    // TODO Uncomment when nonce added to registration API
//                    try {
//                        user.put("phone_verify_nonce", nonce);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                    Intent close = new Intent("EnterPhone");
                    close.putExtra("action", "close");
                    LocalBroadcastManager.getInstance(VerifyPhoneActivity.this).sendBroadcast(close);

                    Intent intent = new Intent(VerifyPhoneActivity.this, YourDetailsActivity.class);
                    intent.putExtra("country", country);
                    intent.putExtra("user", user.toString());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }

                @Override
                public void onError() {
                    progressCircle.setVisibility(View.GONE);

                    editCode.setText("");

                    Toast.makeText(
                            VerifyPhoneActivity.this,
                            getString(R.string.something_wrong),
                            Toast.LENGTH_SHORT
                    ).show();
                }

                @Override
                public void onError(JSONObject error) {
                    progressCircle.setVisibility(View.GONE);

                    editCode.setText("");

                    Toast.makeText(
                            VerifyPhoneActivity.this,
                            GeneralUtils.getJSONErrorMessage(VerifyPhoneActivity.this, error),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            progressCircle.setVisibility(View.GONE);
        }
    }

    public void onNext() {
        String code = editCode.getText().toString();

        if (code.length() != 6) {
            Toast.makeText(
                    VerifyPhoneActivity.this,
                    String.format(getString(R.string.must_enter), "the six digit code you received via SMS"),
                    LENGTH_SHORT
            ).show();
        } else {
            verifyPhone(code);
        }
    }
}
