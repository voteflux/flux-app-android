package org.voteflux.fluxapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.text.SpannableStringBuilder;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.activities.WelcomeActivity;

import java.security.SecureRandom;
import java.util.Random;

public class GeneralUtils {

    public static void goToWelcomeError(Activity activity, String error) {
        Intent intent = new Intent(activity, WelcomeActivity.class);
        intent.putExtra("error", error);
        activity.startActivity(intent);
        activity.finish();
    }

    public static String getJSONErrorMessage(Context context, JSONObject error) {
        try {
            return String.format(context.getString(R.string.something_wrong_reason), error.getString("error"));
        } catch (JSONException e) {
            e.printStackTrace();
            return context.getString(R.string.something_wrong);
        }
    }

    public static SpannableStringBuilder boldSelectedText(Context context, String text, String... wordsToBold) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);

        for (String word : wordsToBold) {
            if (word.length() > 0 && !word.trim().equals("")) {
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();

                if (startIndex >= 0 && endIndex >= 0) {
                    builder.setSpan(new LatoTypefaceSpan(FontCache.getTypeface("fonts/Lato-Heavy.ttf", context)), startIndex, endIndex, 0);
                }
            }
        }

        return builder;
    }

    public static String generateNonce(int size) {
        char[] VALID_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456879".toCharArray();

        SecureRandom srandom = new SecureRandom();
        Random random = new Random();
        char[] buffer = new char[size];

        for (int i = 0; i < size; i++) {
            if ((i % 10) == 0) {
                random.setSeed(srandom.nextLong());
            }

            buffer[i] = VALID_CHARACTERS[random.nextInt(VALID_CHARACTERS.length)];
        }

        return new String(buffer);
    }

    public static String urlifyString(String str) {
        return str.replace(" ", "%20");
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Animation slideUpIn(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.slide_up_in);
    }

    public static Animation slideUpOut(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.slide_up_out);
    }

    public static Animation slideDownIn(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.slide_down_in);
    }

    public static Animation slideDownOut(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.slide_down_out);
    }

    public static Animation fadeIn(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.fade_in);
    }

    public static Animation fadeOut(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }
}
