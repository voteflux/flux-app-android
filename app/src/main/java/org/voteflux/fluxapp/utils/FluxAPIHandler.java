package org.voteflux.fluxapp.utils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;

public class FluxAPIHandler {

    public static void getStreets(String countryCode, String postcode, String suburb, final GetStreetsCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                GeneralUtils.urlifyString(String.format("%1$s/v0/get_streets/%2$s/%3$s/%4$s", Constants.API_URL, countryCode, postcode, suburb)),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void getSuburbs(String countryCode, String postcode, final GetSuburbsCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                String.format("%1$s/v0/get_suburbs/%2$s/%3$s", Constants.API_URL, countryCode, postcode),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void getUser(JSONObject params, final GetUserCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v0/user_details", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void getUserMagicLink(String token, final GetUserMagicLinkCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                String.format("%1$s/v1/app/login_magic_link/%2$s", Constants.API_URL, token),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void registerUser(JSONObject params, final RegisterUserCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v0/register/all_at_once", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void sendFeedback(JSONObject params, final SendFeedbackCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v1/app/feedback", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void sendMagicLink(JSONObject params, final SendMagicLinkCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v1/app/new_magic_link", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        request.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void sendSMS(JSONObject params, final SendSMSCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v1/app_rego/phone_commit", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        request.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void updateUserDetails(JSONObject params, final UpdateUserDetailsCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v0/user_details", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public static void verifyPhone(JSONObject params, final VerifyPhoneCallback callback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format("%1$s/v1/app_rego/phone_verify", Constants.API_URL),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.has("error")) {
                            callback.onError(response);
                        } else {
                            callback.onSuccess(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onError();
                    }
                }
        );

        FluxApp.getInstance().addToRequestQueue(request);
    }

    public interface GetStreetsCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface GetSuburbsCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface GetUserCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface GetUserMagicLinkCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface RegisterUserCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface SendFeedbackCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface SendMagicLinkCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface SendSMSCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface UpdateUserDetailsCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }

    public interface VerifyPhoneCallback {
        void onSuccess(JSONObject result);
        void onError();
        void onError(JSONObject error);
    }
}
