package org.voteflux.fluxapp.utils;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;

import java.util.ArrayList;

public class Constants {
    //public static final String API_URL = "http://flux-api-dev.herokuapp.com/api";
    public static final String API_URL = "https://api.voteflux.org/api";

    private static final ArrayList<JSONObject> countries = new ArrayList<>();
    private static final ArrayList<JSONObject> exampleIssues = new ArrayList<>();

    public static ArrayList<JSONObject> getCountries() {
        if (countries.size() == 0) {
            generateCountries();
        }

        return countries;
    }

    private static void generateCountries() {
        try {
            countries.add(new JSONObject("{" +
                    "\"country\" : \"Australia\"," +
                    "\"countryCode\" : \"au\"," +
                    "\"regionCode\" : \"AUS\"," +
                    "\"mobileCode\" : \"+61\"" +
                "}"
            ));

//            countries.add(new JSONObject("{" +
//                    "\"country\" : \"Brazil\"," +
//                    "\"countryCode\" : \"br\"," +
//                    "\"regionCode\" : \"BRA\"," +
//                    "\"mobileCode\" : \"+55\"" +
//                "}"
//            ));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<JSONObject> getExampleIssues() {
        if (exampleIssues.size() == 0) {
            generateExampleIssues();
        }

        return exampleIssues;
    }

    private static void generateExampleIssues() {
        try {
            exampleIssues.add((new JSONObject("{" +
                    "\"issue\" : \"Same Sex Marriage\"," +
                    "\"issueDescription\" : \"Same Sex Marriage description\"" +
                "}"
            )));

            exampleIssues.add((new JSONObject("{" +
                    "\"issue\" : \"Offshore Refugee Processing\"," +
                    "\"issueDescription\" : \"Offshore Refugee Processing description\"" +
                "}"
            )));

            exampleIssues.add((new JSONObject("{" +
                    "\"issue\" : \"Increasing GST\"," +
                    "\"issueDescription\" : \"Increasing GST description\"" +
                "}"
            )));

            exampleIssues.add((new JSONObject("{" +
                    "\"issue\" : \"Negative Gearing\"," +
                    "\"issueDescription\" : \"Negative Gearing description\"" +
                "}"
            )));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
