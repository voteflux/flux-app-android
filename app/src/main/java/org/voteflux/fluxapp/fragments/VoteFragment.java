package org.voteflux.fluxapp.fragments;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.activities.DashboardActivity;
import org.voteflux.fluxapp.adapters.FluxVoteAdapter;
import org.voteflux.fluxapp.utils.Constants;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class VoteFragment extends Fragment implements DashboardActivity.HideIssueDetailsListener {

    LinearLayout emptyView, moreDetails, issuesContainer;
    SwipeRefreshLayout issuesList;
    RecyclerView listVote;
    ImageView imageBack;
    ImageButton buttonYes, buttonNo;
    TextView textTitle, textDetails;

    FluxVoteAdapter adapter;

    int position;
    boolean showingDetails = false;
    boolean showingEmpty = false;

    public VoteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vote, container, false);

        listVote = (RecyclerView) view.findViewById(R.id.list_vote);

        emptyView = (LinearLayout) view.findViewById(R.id.empty);
        moreDetails = (LinearLayout) view.findViewById(R.id.moreDetails);
        issuesContainer = (LinearLayout) view.findViewById(R.id.issueContainer);

        issuesList = (SwipeRefreshLayout) view.findViewById(R.id.issuesList);

        textTitle = (TextView) view.findViewById(R.id.txt_title);
        textDetails = (TextView) view.findViewById(R.id.txt_moreDetails);

        adapter = new FluxVoteAdapter(getActivity(), Constants.getExampleIssues());
        adapter.setOnItemClickedListener(new FluxVoteAdapter.OnItemClickedListener() {
            @Override
            public void getItemAtPosition(int position) {
                try {
                    VoteFragment.this.position = position;
                    textTitle.setText(adapter.getItem(position).getString("issue"));
                    textDetails.setText(String.format(getString(R.string.example_details), adapter.getItem(position).getString("issue")));
                    toggleDetails();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        listVote.setAdapter(adapter);
        listVote.setLayoutManager(new LinearLayoutManager(getActivity()));

        issuesList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((FluxVoteAdapter) listVote.getAdapter()).addIssues(Constants.getExampleIssues());
                if (showingEmpty) {
                    toggleEmpty();
                }

                issuesList.setRefreshing(false);
            }
        });

        issuesList.setColorSchemeResources(R.color.accent);

        imageBack = (ImageView) view.findViewById(R.id.img_back);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleDetails();
            }
        });

        buttonYes = (ImageButton) view.findViewById(R.id.button_yes);
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUserVoted(position, "yes");
            }
        });

        buttonNo = (ImageButton) view.findViewById(R.id.button_no);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUserVoted(position, "no");
            }
        });

        setSwipingActions();

        return view;
    }

    @Override
    public void hideIssueDetails() {
        if (showingDetails) {
            toggleDetails();
        }
    }

    public void toggleDetails() {

        if (!showingDetails) {
            // Show the panel
            showingDetails = true;

            issuesList.startAnimation(GeneralUtils.slideUpOut(getActivity()));
            issuesList.setVisibility(View.GONE);

            moreDetails.startAnimation(GeneralUtils.slideUpIn(getActivity()));
            moreDetails.setVisibility(View.VISIBLE);
        }
        else {
            // Hide the Panel
            showingDetails = false;

            moreDetails.startAnimation(GeneralUtils.slideDownOut(getActivity()));
            moreDetails.setVisibility(View.GONE);

            issuesList.startAnimation(GeneralUtils.slideDownIn(getActivity()));
            issuesList.setVisibility(View.VISIBLE);
        }
    }

    public void toggleEmpty() {

        if (!showingEmpty) {
            // Show the panel
            showingEmpty = true;

            issuesContainer.startAnimation(GeneralUtils.fadeOut(getActivity()));
            issuesContainer.setVisibility(View.GONE);

            emptyView.startAnimation(GeneralUtils.fadeIn(getActivity()));
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            // Hide the Panel
            showingEmpty = false;

            emptyView.startAnimation(GeneralUtils.fadeOut(getActivity()));
            emptyView.setVisibility(View.GONE);

            issuesContainer.startAnimation(GeneralUtils.fadeIn(getActivity()));
            issuesContainer.setVisibility(View.VISIBLE);
        }
    }

    public boolean isShowingDetails() {
        return showingDetails;
    }

    private void setSwipingActions() {
        ItemTouchHelper.SimpleCallback issueTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    // User votes against
                    try {
                        Toast.makeText(
                                VoteFragment.this.getContext(),
                                String.format(getString(R.string.voted_for), "no", adapter.getItem(position).getString("issue")),
                                Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    adapter.removeIssue(position);

                } else if (direction == ItemTouchHelper.RIGHT){
                    //User votes for
                    try {
                        Toast.makeText(
                                VoteFragment.this.getContext(),
                                String.format(getString(R.string.voted_for), "yes", adapter.getItem(position).getString("issue")),
                                Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    adapter.removeIssue(position);
                }

                checkIfEmpty();
            }

            @Override
            public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                    float dx, float dy, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                Paint paint = new Paint();

                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dx > 0){
                        paint.setColor(ContextCompat.getColor(getActivity(), R.color.yes));
                        RectF background = new RectF(
                                (float) itemView.getLeft(), (float) itemView.getTop(),
                                dx, (float) itemView.getBottom()
                        );

                        canvas.drawRect(background, paint);

                        icon = GeneralUtils.getBitmapFromVectorDrawable(getActivity(), R.drawable.ic_yes);

                        RectF icon_dest = new RectF(
                                (float) itemView.getLeft() + width, (float) itemView.getTop() + width,
                                (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width
                        );

                        canvas.drawBitmap(icon, null, icon_dest, paint);
                    } else if (dx < 0){
                        paint.setColor(ContextCompat.getColor(getActivity(), R.color.no));
                        RectF background = new RectF(
                                (float) itemView.getRight() + dx, (float) itemView.getTop(),
                                (float) itemView.getRight(), (float) itemView.getBottom()
                        );

                        canvas.drawRect(background, paint);

                        icon = GeneralUtils.getBitmapFromVectorDrawable(getActivity(), R.drawable.ic_no);

                        RectF icon_dest = new RectF(
                                (float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width,
                                (float) itemView.getRight() - width, (float)itemView.getBottom() - width
                        );

                        canvas.drawBitmap(icon, null, icon_dest, paint);
                    }
                }
                super.onChildDraw(canvas, recyclerView, viewHolder, dx, dy, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper touchHelper = new ItemTouchHelper(issueTouchCallback);
        touchHelper.attachToRecyclerView(listVote);
    }

    public void onUserVoted(int position, String preference) {
        if (preference.equals("no")) {
            // User votes against
            try {
                Toast.makeText(
                        VoteFragment.this.getContext(),
                        String.format(getString(R.string.voted_for), preference, adapter.getItem(position).getString("issue")),
                        Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.removeIssue(position);

        } else if (preference.equals("yes")){
            //User votes for
            try {
                Toast.makeText(
                        VoteFragment.this.getContext(),
                        String.format(getString(R.string.voted_for), preference, adapter.getItem(position).getString("issue")),
                        Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.removeIssue(position);
        }

        toggleDetails();
        checkIfEmpty();
    }

    private void checkIfEmpty() {
        Log.d(FluxApp.TAG, String.valueOf(listVote.getAdapter() == null || listVote.getAdapter().getItemCount() == 0));
        if (listVote.getAdapter() == null || listVote.getAdapter().getItemCount() == 0) {
            toggleEmpty();
        }
    }
}
