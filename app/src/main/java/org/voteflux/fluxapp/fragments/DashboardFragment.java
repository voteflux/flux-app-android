package org.voteflux.fluxapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.utils.GeneralUtils;

public class DashboardFragment extends Fragment {

    TextView textThankYou;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        textThankYou = (TextView) view.findViewById(R.id.txt_thankYou);
        textThankYou.setText(
                GeneralUtils.boldSelectedText(DashboardFragment.this.getContext(), getString(R.string.thank_you), "Thanks")
        );

        return view;
    }

}
