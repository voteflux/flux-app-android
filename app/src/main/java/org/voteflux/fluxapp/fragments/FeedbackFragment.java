package org.voteflux.fluxapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;
import org.voteflux.fluxapp.activities.SplashScreenActivity;
import org.voteflux.fluxapp.utils.FluxAPIHandler;
import org.voteflux.fluxapp.utils.GeneralUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedbackFragment extends Fragment {

    EditText editFeedback;
    Button buttonFeedbuck;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        setUpView(view);

        return view;
    }

    private void setUpView(View view) {
        editFeedback = (EditText) view.findViewById(R.id.edit_feedback);

        buttonFeedbuck = (Button) view.findViewById(R.id.button_feedback);
        buttonFeedbuck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedback = editFeedback.getText().toString();

                if (feedback.isEmpty()) {
                    Toast.makeText(
                            FeedbackFragment.this.getContext(),
                            String.format(getString(R.string.must_enter), "your feedback"),
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    JSONObject params = new JSONObject();
                    try {
                        params.put("s", FluxApp.getSharedPrefs().getString("s", ""));
                        params.put("feedback", feedback);

                        FluxAPIHandler.sendFeedback(params, new FluxAPIHandler.SendFeedbackCallback() {
                            @Override
                            public void onSuccess(JSONObject result) {
                                if (result.has("error")) {
                                    Toast.makeText(
                                            FeedbackFragment.this.getContext(),
                                            GeneralUtils.getJSONErrorMessage(FeedbackFragment.this.getContext(), result),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                } else {
                                    editFeedback.setText("");
                                    Toast.makeText(
                                            FeedbackFragment.this.getContext(),
                                            getString(R.string.thanks_for_feedback),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }

                            @Override
                            public void onError() {
                                Toast.makeText(
                                        FeedbackFragment.this.getContext(),
                                        getString(R.string.something_wrong),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }

                            @Override
                            public void onError(JSONObject error) {
                                Toast.makeText(
                                        FeedbackFragment.this.getContext(),
                                        GeneralUtils.getJSONErrorMessage(FeedbackFragment.this.getContext(), error),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
