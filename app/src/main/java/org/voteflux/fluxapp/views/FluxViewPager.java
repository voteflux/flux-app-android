package org.voteflux.fluxapp.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * This is a custom view pager class that is able to disable swiping
 */

public class FluxViewPager extends ViewPager{
    private boolean swipingEnabled;

    public FluxViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        swipingEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return swipingEnabled ? super.onTouchEvent(event) : false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return swipingEnabled ? super.onInterceptTouchEvent(event) : false;
    }

    public void setSwipingEnabled(boolean enabled) {
        this.swipingEnabled = enabled;
    }

    public boolean isSwipingEnabled() {
        return swipingEnabled;
    }
}
