package org.voteflux.fluxapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import org.voteflux.fluxapp.utils.FontCache;

public class LatoLightButton extends Button {
    public LatoLightButton(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public LatoLightButton(Context context, AttributeSet attributes) {
        super(context, attributes);
        applyCustomFont(context);
    }

    public LatoLightButton(Context context, AttributeSet attributes, int defStyle) {
        super(context, attributes, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/Lato-Light.ttf", context);
        setTypeface(customFont);
    }
}
