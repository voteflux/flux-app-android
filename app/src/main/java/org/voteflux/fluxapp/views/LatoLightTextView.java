package org.voteflux.fluxapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import org.voteflux.fluxapp.utils.FontCache;

public class LatoLightTextView extends TextView {
    public LatoLightTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public LatoLightTextView(Context context, AttributeSet attributes) {
        super(context, attributes);
        applyCustomFont(context);
    }

    public LatoLightTextView(Context context, AttributeSet attributes, int defStyle) {
        super(context, attributes, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/Lato-Light.ttf", context);
        setTypeface(customFont);
    }
}
