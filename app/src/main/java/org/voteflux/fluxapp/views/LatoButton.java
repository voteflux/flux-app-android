package org.voteflux.fluxapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import org.voteflux.fluxapp.utils.FontCache;

public class LatoButton extends Button {
    public LatoButton(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public LatoButton(Context context, AttributeSet attributes) {
        super(context, attributes);
        applyCustomFont(context);
    }

    public LatoButton(Context context, AttributeSet attributes, int defStyle) {
        super(context, attributes, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/Lato-Regular.ttf", context);
        setTypeface(customFont);
    }
}
