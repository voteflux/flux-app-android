package org.voteflux.fluxapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import org.voteflux.fluxapp.utils.FontCache;

public class LatoLightEditText extends EditText {
    public LatoLightEditText(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public LatoLightEditText(Context context, AttributeSet attributes) {
        super(context, attributes);
        applyCustomFont(context);
    }

    public LatoLightEditText(Context context, AttributeSet attributes, int defStyle) {
        super(context, attributes, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/Lato-Light.ttf", context);
        setTypeface(customFont);
    }
}
