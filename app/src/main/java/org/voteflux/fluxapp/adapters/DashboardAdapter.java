package org.voteflux.fluxapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.voteflux.fluxapp.fragments.DashboardFragment;
import org.voteflux.fluxapp.fragments.FeedbackFragment;
import org.voteflux.fluxapp.fragments.VoteFragment;

import java.util.ArrayList;
import java.util.List;

public class DashboardAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> fragmentList = new ArrayList<>();

    public DashboardAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public void addFragment(Fragment fragment) {
        fragmentList.add(fragment);
    }
}
