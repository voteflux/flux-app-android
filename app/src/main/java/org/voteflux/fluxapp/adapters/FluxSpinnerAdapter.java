package org.voteflux.fluxapp.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.voteflux.fluxapp.R;

import java.util.ArrayList;

public class FluxSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> content;

    public FluxSpinnerAdapter(Context context, ArrayList<String> content) {
        this.content = content;
        activity = context;
    }

    @Override
    public int getCount() {
        return content.size();
    }

    @Override
    public Object getItem(int position) {
        return content.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.view_flux_spinner_row, null);
        }

        TextView option = (TextView) view.findViewById(R.id.txt_option);
        option.setText(content.get(position));

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.view_flux_spinner, null);
        }

        TextView option = (TextView) view.findViewById(R.id.txt_option);
        option.setText(content.get(position));

        switch (content.get(position)) {
            case "Suburb":
            case "Street Name":
            case "Loading...":
                option.setTextColor(ContextCompat.getColor(activity, R.color.placeholder));
                break;
            default:
                option.setTextColor(ContextCompat.getColor(activity, R.color.text));
                break;
        }

        return view;
    }
}
