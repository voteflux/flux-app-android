package org.voteflux.fluxapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.FluxApp;
import org.voteflux.fluxapp.R;

import java.util.List;

public class FluxVoteAdapter extends RecyclerView.Adapter<FluxVoteAdapter.ViewHolder> {

    private Context context;
    private List<JSONObject> issues;

    private OnItemClickedListener listener;

    public FluxVoteAdapter(Context context, List<JSONObject> issues) {
        this.context = context;
        this.issues = issues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context viewContext = parent.getContext();

        View view = LayoutInflater.from(viewContext).inflate(R.layout.view_vote_row, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        JSONObject issue = issues.get(position);

        LinearLayout row = holder.row;
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(FluxApp.TAG, String.valueOf(holder.getAdapterPosition()));
                listener.getItemAtPosition(holder.getAdapterPosition());
            }
        });

        TextView textIssue = holder.textIssue;
        try {
            textIssue.setText(issue.getString("issue"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return issues.size();
    }

    public Context getContext() {
        return context;
    }

    public void setOnItemClickedListener(OnItemClickedListener listener) {
        this.listener = listener;
    }

    public JSONObject getItem(int position) {
        return issues.get(position);
    }

    public void removeIssue(int position) {
        issues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, issues.size());
    }

    public void addIssues(List<JSONObject> issues) {
        this.issues = issues;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout row;
        public TextView textIssue;

        public ViewHolder(View itemView) {
            super(itemView);

            row = (LinearLayout) itemView.findViewById(R.id.vote_row);
            textIssue = (TextView) itemView.findViewById(R.id.txt_issue);
        }
    }

    public interface OnItemClickedListener {
        void getItemAtPosition(int position);
    }
}
