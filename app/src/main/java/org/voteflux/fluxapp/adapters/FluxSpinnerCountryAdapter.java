package org.voteflux.fluxapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.voteflux.fluxapp.R;

import java.util.ArrayList;

public class FluxSpinnerCountryAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<JSONObject> countries;

    public FluxSpinnerCountryAdapter(Context context, ArrayList<JSONObject> countries) {
        this.countries = countries;
        activity = context;
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public JSONObject getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.view_flux_spinner_row, null);
        }

        TextView option = (TextView) view.findViewById(R.id.txt_option);

        try {
            option.setText(countries.get(position).getString("country"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.view_flux_spinner, null);
        }

        TextView option = (TextView) view.findViewById(R.id.txt_option);

        try {
            option.setText(countries.get(position).getString("country"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
